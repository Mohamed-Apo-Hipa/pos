<?php

namespace App\Http\Controllers\Dashboard;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_products'])->only('index');
        $this->middleware(['permission:create_products'])->only('create');
        $this->middleware(['permission:update_products'])->only('edit');
        $this->middleware(['permission:delete_products'])->only('destroy');
    }

  
    public function index(Request $request)
    {
        $categories = Category::all();
        $request->search != null ? $paginate = 1 : $paginate = 5;
        
        $products = Product::when($request->search, function($q) use ($request){

            return $q->whereTranslationLike('name', '%' . $request->search . '%');
    
        })->when($request->category_id, function($q) use ($request){

            return $q->where('category_id',$request->category_id);

        }) ->latest()->paginate($paginate); //get categories and make a search;
    

        return view('dashboard.products.index',compact('products','categories'));
    }

    
    public function create()
    {
        $categories = Category::all();
        return view('dashboard.products.create',compact('categories'));
    }

   
    public function store(Request $request)
    {
        $rules = [
            'category_id' => 'required'
        ];
        foreach (config('translatable.locales') as $locale)
        {
            $rules+=[$locale .'.name' => 'required|unique:product_translations,name'];
            $rules+=[$locale .'.description' => 'required'];
        }
        $rules+=[
            'purchase_price' => 'required',
            'sale_price' => 'required',
            'stock' => 'required',
        ];

        $request->validate($rules,[],[
            'category_id' =>__('site.category_id'),
            'ar.description' =>__('site.ar.productDescription'),
            'en.description' =>__('site.en.productDescription'),
            'purchase_price' => __('site.purchase_price'),
            'sale_price'     => __('site.sale_price'),
            'stock'          => __('site.stock'),
        ]);

        $data = $request->all();
        if($request->image)
        {
            \Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/product_images/'. $request->image->hashName()));

            $data['image'] = $request->image->hashName();
        }

        Product::create($data);
        session()->flash('success',__('site.added_successfully'));
        return redirect()->route('dashboard.products.index');
    }


    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('dashboard.products.edit',compact('product','categories'));
    }


   
    public function update(Request $request, Product $product)
    {
        $rules = [
            'category_id' => 'required'
        ];
        foreach (config('translatable.locales') as $locale)
        {

            $rules+= [$locale.'.name' => ['required', Rule::unique('product_translations','name')->ignore($product->id , 'product_id')]];
            $rules+=[$locale .'.description' => 'required'];
        }
        $rules+=[
            'purchase_price' => 'required',
            'sale_price' => 'required',
            'stock' => 'required',
        ];

        $request->validate($rules,[],[
            'category_id' =>__('site.category_id'),
            'ar.description' =>__('site.ar.productDescription'),
            'en.description' =>__('site.en.productDescription'),
            'purchase_price' => __('site.purchase_price'),
            'sale_price'     => __('site.sale_price'),
            'stock'          => __('site.stock'),
        ]);

        $data = $request->all();
        if($request->image)
        {
            if($product->image != 'default.png')
        {
            Storage::disk('public_uploads')->delete('/product_images/' . $product->image );
            
        }
            \Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/product_images/'. $request->image->hashName()));

            $data['image'] = $request->image->hashName();
        }

        $product->update($data);
        session()->flash('success',__('site.updated_successfully'));
        return redirect()->route('dashboard.products.index');
    }

   
    public function destroy(Product $product)
    {
        if($product->image != 'default.png')
        {
            Storage::disk('public_uploads')->delete('/product_images/' . $product->image );
        }
        $product->delete();
        session()->flash('success',__('site.deleted_successfully'));
        return back();
    }
}
