<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_orders'])->only('index');
        $this->middleware(['permission:create_orders'])->only('create');
        $this->middleware(['permission:update_orders'])->only('edit');
        $this->middleware(['permission:delete_orders'])->only('destroy');
    }

    public function index(Request $request)
    {

        $request->search != null ? $paginate = 1 : $paginate = 5;
        $orders = Order::whereHas('client' , function($q) use($request){

            return $q->where('name', 'like' , '%'  . $request->search . '%');

        }) ->latest()->paginate($paginate); //get orders and make a search;
     
        return view('dashboard.orders.index', compact('orders'));

    } // end of index

    public function products(Order $order)
    {

        $products =   $order->products()->get();

        return view('dashboard.orders._products', compact('products','order'));

    } //end of products

    public function destroy(Order $order){

        
        foreach ($order->products as $product) 
        {
            $product->update([
                'stock' => $product->stock + $product->pivot->quantities
            ]);
        }
        
        $order->delete();
        session()->flash('success',__('site.deleted_successfully'));
        return back();
    }

} // end of controller 
