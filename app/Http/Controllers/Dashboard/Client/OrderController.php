<?php

namespace App\Http\Controllers\Dashboard\Client;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use App\Category;
use App\Product;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:create_orders'])->only('create');
        $this->middleware(['permission:update_orders'])->only('edit');
        $this->middleware(['permission:delete_orders'])->only('destroy');
    }
    
    public function create(Client $client)
    {
        $categories  = Category::with('products')->get();

        $orders = $client->orders()->with('products')->paginate(5);
        return view('dashboard.clients.orders.create', compact('client','categories','orders'));
    }

   
    public function store(Request $request ,Client $client)
    {
        $request->validate([

            'products' => 'required|array',
        ]);

            $this->attach_order($request, $client);

        session()->flash('success',__('site.added_successfully'));
        return redirect()->route('dashboard.orders.index');
    
    }

 
  
    public function edit(Client $client ,Order $order)
    {
        
        $categories  = Category::with('products')->get();
        $orders = $client->orders()->with('products')->paginate(5);
        return view('dashboard.clients.orders.edit',compact('order','client','categories','orders'));
    }

   
    public function update(Request $request, Client $client , Order $order)
    {
        $request->validate([

            'products' => 'required|array',
        ]);

        $this->detach_order($order);

        $this->attach_order($request, $client);

        session()->flash('success',__('site.added_successfully'));
        return redirect()->route('dashboard.orders.index');
    
    }

    private function  attach_order($request , $client)
    {

        $order = $client->orders()->create([]);

        $order->products()->attach($request->products);
        
        $total_Price = 0 ;
        
        foreach ($request->products as $id=>$quantity) {

            $product = Product::findOrFail($id);

            $total_Price+= $product->sale_price * $quantity['quantities']; 

            $product->update([ 'stock' => $product->stock - $quantity['quantities'] ]);

        } //end of foreach

        $order->update([

            'total_price' =>  $total_Price
        ]);

    } //end of private attach_order

    private function detach_order($order){

           
        foreach ($order->products as $product) 
        {
            $product->update([
                'stock' => $product->stock + $product->pivot->quantities
            ]);
        }
        
        $order->delete();
    }
}
