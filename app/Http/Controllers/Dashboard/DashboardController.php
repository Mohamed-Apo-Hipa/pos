<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(){

        $categories_count = Category::count();
        $products_count = Product::count();
        $clients_count = Client::count();
        $users_count = User::whereRoleIs('admin')->count();

        $sales_data = Order::select(
            DB::raw('YEAR(created_at) AS year'),
            DB::raw('month(created_at) AS month'),
            DB::raw('sum(total_price) AS sum')
        )->groupBy('month')->get();

        return view('dashboard.index',compact('categories_count','products_count','clients_count','users_count','sales_data'));
        
    } //end of index
}
