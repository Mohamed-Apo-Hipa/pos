<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
    protected $guarded = [];
    use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = ['name'];

    public function products()
    { 

        return $this->hasMany(Product::class);

    } //end of products
    
}
