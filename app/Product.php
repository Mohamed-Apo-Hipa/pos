<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    protected $guarded = [];
    use \Dimsav\Translatable\Translatable;
    public $translatedAttributes = ['name', 'description'];
    protected $appends  = ['image_path', 'profit_percent'];

    public function category()
    {
        return $this->BelongsTo(Category::class);
    } //end of category


    public function orders()
    {
        return $this->belongsToMany(Order::class, 'product_order');
    } //end of orders

    public function getImagePathAttribute()
    {
        return asset('uploads/product_images/' . $this->image);
    } //end of get image

    public function getProfitPercentAttribute()
    {
        $profit = $this->sale_price - $this->purchase_price;
        $profit_percent  = $profit * 100 / $this->purchase_price;
        return  number_format($profit_percent, 2);
    }

    // public function getSalePriceAttribute($price)
    // {
    //     if ($price > 1000) {
    //         return $price / 1000 . ' K';
    //     } elseif ($price > 1000000) {
    //         return $price / 1000 . ' M';
    //     } else {
    //         return $price;
    //     }
    // }
}
