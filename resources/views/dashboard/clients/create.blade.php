@extends('layouts.dashboard.app')
@section('content')
  <div class="content-wrapper">
    <section class="content-header">
      <h1> @lang('site.add') @lang('site.clients') </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-th"></i>@lang('site.dashboard')</a></li>
        <li><a href="{{ route('dashboard.clients.index') }}"> @lang('site.clients') </a></li>
        <li class="active">@lang('site.add')</li>
      </ol>
    </section>
    <section class="content container-fluid">
      <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title"> <i class="fa fa-plus-circle"></i> @lang('site.add')</h3>
        </div> {{-- end of box header --}}
        <div class="box-body">

          @include('partials._errors')
        
          {!! Form::open(['method'=>'post', 'route'=>'dashboard.clients.store']) !!}

            <div class="form-group">
                <label>@lang('site.clientName')</label>
                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
            </div>
            @for ($i = 0; $i < 2; $i++)   

              <div class="form-group">
                  <label>@lang('site.phone')</label>
                  <input type="text" name="phone[]" class="form-control" >
              </div>

            @endfor
            <div class="form-group">
                <label>@lang('site.address')</label>
                <textarea class="form-control" name="address"> {{ old('address') }} </textarea>
            </div>
           
            <div class="form-group">
               <button type="submit" class="btn btn-success"><i class="fa fa-plus-circle"></i> @lang('site.add')</button>
            </div> 

          {!! Form::close() !!}
        </div>{{-- end of box body --}}
      </div> {{-- end of box --}}

    </section>
  </div>
@endsection