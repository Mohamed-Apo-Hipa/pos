@extends('layouts.dashboard.app')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1> @lang('site.add') @lang('site.orders') </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-th"></i>@lang('site.dashboard')</a></li>
            <li><a href="{{ route('dashboard.clients.index') }}"> @lang('site.clients') </a></li>
            <li class="active">@lang('site.add_order')</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"> <i class="fa fa-list-alt"></i> @lang('site.categories')</h3>
                    </div> {{-- end of box header  --}}
                    <div class="box-body">
                        @foreach ($categories as $category)
                        <div class="panel-group">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" href="#{{ str_replace(' ','_', $category->name) }}">
                                        <h4 class="panel-title text-center">
                                            <i class="fa fa-arrow-circle-down"></i>
                                            {{ $category->name }}
                                            <i class="fa fa-arrow-circle-up"></i>
                                        </h4>
                                    </a>
                                </div>

                                <div class="panel-collapse collapse" id="{{ str_replace(' ','_', $category->name) }}">
                                    <div class="panle-body">

                                        @if ($category->products->count() > 0)
                                        <table class="table table-hover">
                                            <tr>
                                                <th>@lang('site.name')</th>
                                                <th>@lang('site.stock')</th>
                                                <th>@lang('site.price')</th>
                                                <th>@lang('site.add')</th>
                                            </tr>
                                            @foreach ($category->products as $product)
                                            <tr>
                                                <td>{{ $product->name }}</td>
                                                <td>{{ $product->stock }}</td>
                                                <td>{{ number_format($product->sale_price, 2) }}</td>
                                                <td>
                                                    <a href="" id="product-{{ $product->id }}"
                                                        data-name="{{ $product->name }}" data-id="{{ $product->id }}"
                                                        data-price="{{ $product->sale_price }}"
                                                        style="border-radius: 50%;"
                                                        class="btn btn-info btn-sm add-product-btn">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                        @else
                                        <div class="alert alert-info text-center" style="margin: 20px auto">

                                            <i class="fa fa-hand-pointer-o fa-lg">
                                                <a style="font-size: 20px;text-decoration: none;border: none;"
                                                    class="btn btn-info btn-lg"
                                                    href="{{ route('dashboard.products.index') }}">@lang('site.putSomeProduct')
                                                </a>
                                            </i>
                                        </div>
                                        @endif
                                    </div> <!-- end of panel body -->

                                </div><!-- end of panel collapse -->

                            </div><!-- end of panel primary -->

                        </div><!-- end of panel group -->

                        @endforeach {{-- end category panel --}}

                    </div>{{-- end of box body --}}

                </div>{{--end of box for category --}}

            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"> <i class="fa fa-plus-circle"></i> @lang('site.orders')</h3>
                    </div> {{-- end of box header for order --}}
                    <div class="box-body">
                        {!! Form::open(['route'=>['dashboard.clients.orders.store', $client->id], 'method' => 'post'])
                        !!}
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>@lang('site.product')</th>
                                    <th>@lang('site.quantity')</th>
                                    <th>@lang('site.price')</th>
                                </tr>
                            </thead>

                            <tbody class="order-list">

                                {{-- get data from file order.js --}}

                            </tbody>
                        </table><!-- end of table -->
                        <h4> @lang('site.total') : <span class="total-price">0</span></h4>
                        <button class="btn btn-primary btn-block" style="display:none" id="add-order-form-btn"><i
                                class="fa fa-plus"></i> @lang('site.add_order') </button>
                        {!! Form::close() !!}

                    </div>{{-- end of box body for order --}}
                </div> {{--end of box for order --}}

            {{--  History orders for client  --}}
                @if ($client->orders->count() > 0)
                    <div class="box box-primary">

                        <div class="box-header">

                            <h3 class="box-title" style="margin-bottom: 10px">@lang('site.previous_orders')
                                <small>{{ $orders->total() }}</small>
                            </h3>

                        </div><!-- end of box header -->

                        <div class="box-body">

                            @foreach ($orders as $order)

                            <div class="panel-group">

                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                    <a data-toggle="collapse" href="#{{ $order->created_at->format('d-m-Y-s') }}">
                                            <h4 class="panel-title">
                                                {{ $order->created_at->toFormattedDateString() }}
                                                
                                            </h4>
                                        </a >
                                    </div>

                                    <div id="{{ $order->created_at->format('d-m-Y-s') }}" class="panel-collapse collapse">

                                        <div class="panel-body">

                                            <ul class="list-group">
                                                @foreach ($order->products as $product)
                                                <li class="list-group-item">{{ $product->name }}</li>
                                                @endforeach
                                            </ul>

                                        </div><!-- end of panel body -->

                                    </div><!-- end of panel collapse -->

                                </div><!-- end of panel primary -->

                            </div><!-- end of panel group -->

                            @endforeach

                            {{ $orders->links() }}

                        </div><!-- end of box body -->

                    </div><!-- end of box -->

                @endif
            </div>

        </div>

</div> {{-- end of box --}}

</section>
@endsection
