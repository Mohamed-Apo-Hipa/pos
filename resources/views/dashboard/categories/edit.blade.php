@extends('layouts.dashboard.app')
@section('content')
  <div class="content-wrapper">
    <section class="content-header">
      <h1> @lang('site.edit') @lang('site.categories') </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-th"></i>@lang('site.dashboard')</a></li>
        <li><a href="{{ route('dashboard.categories.index') }}"> @lang('site.categories') </a></li>
        <li class="active">@lang('site.edit')</li>
      </ol>
    </section>
    <section class="content container-fluid">
      <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title"> <i class="fa fa-edit"></i> @lang('site.add')</h3>
        </div> {{-- end of box header --}}
        <div class="box-body">

          @include('partials._errors')
        
          {!! Form::open(['method'=>'PuT', 'route'=>['dashboard.categories.update', $category->id] ]) !!}

          @foreach (config('translatable.locales') as $locale)
                      
            <div class="form-group">                   
                <label><i class="fa fa-list"> |</i> @lang('site.'. $locale . '.categoryName')</label>
                <input type="text" name="{{ $locale }}[name]" value="{{ $category->translate($locale)->name }}" class="form-control">
            </div> 
          
          @endforeach
            <div class="form-group">
               <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> @lang('site.edit')</button>
            </div> 

          {!! Form::close() !!}
        </div>{{-- end of box body --}}
      </div> {{-- end of box --}}

    </section>
  </div>
@endsection