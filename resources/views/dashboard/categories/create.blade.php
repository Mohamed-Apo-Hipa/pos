@extends('layouts.dashboard.app')
@section('content')
  <div class="content-wrapper">
    <section class="content-header">
      <h1> @lang('site.add') @lang('site.categories') </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-th"></i>@lang('site.dashboard')</a></li>
        <li><a href="{{ route('dashboard.categories.index') }}"> @lang('site.categories') </a></li>
        <li class="active">@lang('site.add')</li>
      </ol>
    </section>
    <section class="content container-fluid">
      <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title"> <i class="fa fa-plus-circle"></i> @lang('site.add')</h3>
        </div> {{-- end of box header --}}
        <div class="box-body">

          @include('partials._errors')
        
          {!! Form::open(['method'=>'post', 'route'=>'dashboard.categories.store','id'=>'form_data']) !!}

            @foreach (config('translatable.locales') as $locale)
                      
                <div class="form-group">                   
                    <label><i class="fa fa-list"> |</i> @lang('site.'. $locale . '.categoryName')</label>
                    <input type="text" name="{{ $locale }}[name]" value="{{ old($locale . '.name') }}" class="form-control">
                </div> 
                
            @endforeach
            <div class="form-group">
               <button type="submit" class="btn btn-success btn-sm" id="btn" ><i class="fa fa-plus-circle"></i> @lang('site.add')</button>
            </div> 

          {!! Form::close() !!}
        </div>{{-- end of box body --}}
      </div> {{-- end of box --}}

    </section>
  </div>
  @push('scripts')
  <script>
      $('#btn').on('click', function(){
          var url = $('#form_data').attr('action');
          var form = $('#form_data').serialize();
          $.ajax({
            url:url,
            data:form,
            dataType:'json',
            type:'post',
            success: function(data){
              new Noty({
                  type: 'success',
                  layout: 'topRight',
                  text: 'added successfully',
                  timeout: 3000,
                  progressbar:true,
                  }).show();

                window.location ="{{ route('dashboard.categories.index') }}";
            },
            error : function(data_error,exception)
            {
              $.each(data_error.responseJSON.errors, function(index,v){
                new Noty({
                type: 'error',
                layout: 'topRight',
                text: v,
                timeout: 3000,
                progressbar:true,
                }).show();
              });
            }
          });
        return false;
      });
  </script>    
  @endpush
@endsection