@extends('layouts.dashboard.app')
@section('content')
  <div class="content-wrapper">
    <section class="content-header">
      <h1> @lang('site.add') @lang('site.products') </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-th"></i>@lang('site.dashboard')</a></li>
        <li><a href="{{ route('dashboard.products.index') }}"> @lang('site.products') </a></li>
        <li class="active">@lang('site.edit')</li>
      </ol>
    </section>
    <section class="content container-fluid">
      <div class="box box-primary">

        <div class="box-header with-border">
            <h3 class="box-title"> <i class="fa fa-edit"></i> @lang('site.add')</h3>
        </div> {{-- end of box header --}}
        <div class="box-body">

          @include('partials._errors')
        
          {!! Form::open(['method'=>'PUT', 'route'=>['dashboard.products.update', $product->id] , 'enctype'=> 'multipart/form-data']) !!}

          <label><i class="fa fa-list-alt"></i> @lang('site.categories')</label>
          <select name="category_id" class="form-control" style="height: 42px">

            <option value="">@lang('site.allCategories')</option>
            
            @foreach ($categories as $category)
                <option value="{{ $category->id  }}"  {{ $product->category_id == $category->id ? 'selected' : '' }} >{{ $category->name }}</option>
            @endforeach
          </select>

            @foreach (config('translatable.locales') as $locale)
                      
                <div class="form-group">                   
                    <label><i class="fa fa-product-hunt"> |</i> @lang('site.'. $locale . '.productName')</label>
                    <input type="text" name="{{ $locale }}[name]" value="{{ $product->translate($locale)->name }}" class="form-control">
                </div> 

                <div class="form-group">                   
                    <label><i class="fa fa-product-hunt"> |</i> @lang('site.'. $locale . '.productDescription')</label>
                    <textarea name="{{ $locale }}[description]" class="form-control ckeditor">{{ $product->translate($locale)->description }}</textarea>
                </div> 
                
            @endforeach


            <div class="row">
              <div class="col-md-3">
                  <div class="form-group">
                      <label for="image"><i class="fa fa-file-image-o"> |</i> @lang('site.image')</label>
                      <input type="file" name="image" class="form-control image">
                  </div> 
                  <div class="form-group">
                      <img src="{{ $product->image_path }}" alt="user image" class="img-thumbnail image-preview" width="100px">
                  </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                    <label for="purchase_price"></i> @lang('site.purchase_price')</label>
                    <input type="number" step="0.01" name="purchase_price" class="form-control" value="{{ $product->purchase_price }}">
                </div> 
              </div>
              <div class="col-md-3">
                <div class="form-group">
                    <label for="sale_price"></i> @lang('site.sale_price')</label>
                    <input type="number" step="0.01" name="sale_price" class="form-control" value="{{ $product->sale_price }}">
                </div> 
              </div>
              <div class="col-md-3">
                <div class="form-group">
                    <label for="stock"></i> @lang('site.stock')</label>
                    <input type="number" name="stock" class="form-control" value="{{ $product->stock }}">
                </div> 
              </div>
            </div>
            <div class="form-group">
               <button type="submit" class="btn btn-success" ><i class="fa fa-edit"></i> @lang('site.update')</button>
            </div> 

          {!! Form::close() !!}
        </div>{{-- end of box body --}}
      </div> {{-- end of box --}}

    </section>
  </div>
@endsection