<?php

use Illuminate\Database\Seeder;
use App\Client;
class clientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $clients = ['Mohamed' ,'Adel'];

        foreach ($clients as $client)
        {
            Client::create([
                'name'    => $client,
                'phone'   => '0100000005',
                'address' =>'faqus',
            ]);

        } // end of foreach
    }
}
