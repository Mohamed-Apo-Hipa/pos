<?php

use Illuminate\Database\Seeder;
use App\Product;

class productsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = ['pro one' ,'pro two' ,'pro Three'];

        foreach ($products as $index=>$product)
        {
            Product::create([
                'category_id'   => '1',
                'ar' => ['name' => $product.$index++ ,'description' =>  $product . 'Desc' . $index ] ,
                'en' => ['name' => $product.$index++  ,'description' =>  $product . 'Desc' . $index ] ,
                'purchase_price'=>150 ,
                'sale_price'    =>200,
                'stock'         =>30
            ]);

        } // end of foreach
    }
}
