<?php

use Illuminate\Database\Seeder;
use App\Category;

class categoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['cat one' ,'cat two' ,'cat Three'];

        foreach ($categories as $category)
        {
            Category::create([
                'ar' => ['name' => $category ] ,
                'en' => ['name' => $category ] ,
            ]);

        } // end of foreach
    }
}
