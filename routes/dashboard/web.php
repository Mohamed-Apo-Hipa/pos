<?php

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]],
    function(){
        Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function(){

            Route::get('/index','DashboardController@index')->name('index');

            //users Routes
            Route::resource('users', 'UserController')->except(['show']);

            //categories Routes
            Route::resource('categories', 'CategoryController')->except(['show']);

            //products Routes
            Route::resource('products', 'ProductController')->except(['show']);

            //clients Routes
            Route::resource('clients', 'ClientController')->except(['show']);
            Route::resource('clients.orders', 'Client\OrderController')->except(['show']);

            //order Routes
            Route::resource('orders', 'OrderController');
            Route::get('/orders/{order}/products', 'OrderController@products')->name('orders.products');
    
        }); //end of dashboard routes
    });

 